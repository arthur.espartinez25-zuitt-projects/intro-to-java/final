package com.zuitt;

import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();
    public Contact(){}
    public Contact(String name, String contactNumber, String contactAddress){
        this.name = name;
        this.numbers.add(contactNumber);
        this.addresses.add(contactAddress);
    }
    public String getName() { return name; }

    public ArrayList<String> getNumbers() { return numbers; }

    public void setNumber(String newNumber) { this.numbers.add(newNumber); }

    public ArrayList<String> getAddresses() { return addresses; }

    public void setAddress(String newAddress) { this.addresses.add(newAddress); }
}