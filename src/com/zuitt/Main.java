package com.zuitt;

import java.util.List;
public class Main
{
    public static void main(String args[])
    {
        PhoneBook phonebook=new PhoneBook();

        Contact person1 = new Contact("Arthur", "0912", "Legazpi");
        person1.setNumber("1234");
        person1.setNumber("6534");

        Contact person2 = new Contact("Blue", "0913", "Albay");
        person2.setNumber("4554");
        person2.setNumber("2344");

        phonebook.setPhoneBook(person1);
        phonebook.setPhoneBook(person2);
        List<Contact>obj=phonebook.getPhoneBook();
        for(Contact temp:obj)
        {
            System.out.println("Name:"+temp.getName());
            System.out.println("Numbers:"+temp.getNumbers());
            System.out.println("Addresses:"+temp.getAddresses());
            System.out.println();
        }
    }
}
